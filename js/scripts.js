if (window.location.pathname == '/create') {
  var productTypes = document.getElementById('productTypes');
  //Form > type select event listener
  productTypes.addEventListener('change', ConditionalFilds);
  function ConditionalFilds() {
    //Hide unneccessary filds and make them not required
    let c = document.getElementById('typeSpecs').children;
    for (let i = 0; i < c.length; i++) {
      c[i].style.display = 'none';
    }
    let input = document
      .getElementById('typeSpecs')
      .getElementsByTagName('input');
    for (i = 0; i < input.length; i++) {
      input[i].required = false;
    }
    //Show neccessary filds and make them required
    let g = document.getElementById(productTypes.value);
    g.style.display = 'block';
    input = g.getElementsByTagName('input');
    for (i = 0; i < input.length; i++) {
      input[i].required = true;
    }
  }

  //For aditional validation if neccessary
  function validateForm() {
    let f = document.forms['create'];
    return true;
  }
}
