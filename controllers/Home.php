<?php

class Home
{
    /**
     * Index page
     */
    public static function Index()
    {
        //Get all products
        $products = array_merge(Book::getAll(), Disc::getAll(), Furniture::getAll());

        //Sort by date
        usort($products, function ($a, $b) {
            return strtotime($b->created_at) - strtotime($a->created_at);
        });

        //Post Conditional
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($_POST['action'] == 'delete') {
                //Mass delete
                foreach ($_POST['products'] as $i) {
                    $products[$i]->deleteMeFromDb();
                }
                //Refresh
                header('Location: /');
            } elseif ($_POST['action'] == 'create') {
                //Redirect to creade page
                header('Location: /create');
            }
        }

        //Views
        require_once LAYOUTS . 'header.php';
        require_once VIEWS . 'list.php';
        require_once LAYOUTS . 'footer.php';
    }

    /**
     * Create page
     */
    public static function Create()
    {
        $valid = ['book', 'furniture', 'disc'];

        //Post Conditional
        if ($_SERVER["REQUEST_METHOD"] == "POST") {

            if (in_array($_POST['type'], $valid)) {
                $p = new $_POST['type'];
                $p->insertMeIntoDb();
            }

            //Refresh
            header('Location: /');
        }

        //Views
        require_once LAYOUTS . 'header.php';
        require_once VIEWS . 'create.php';
        require_once LAYOUTS . 'footer.php';
    }
}
