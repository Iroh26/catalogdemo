<?php

abstract class Product extends Dbh
{
    public $id;
    public $sku;
    public $name;
    public $price;
    public $created_at;

    function __construct()
    {
        $this->id = isset($_POST['id']) ? $_POST['id'] : null;
        $this->sku = isset($_POST['sku']) ? $_POST['sku'] : null;
        $this->name = isset($_POST['name']) ? $_POST['name'] : null;
        $this->price = isset($_POST['price']) ? $_POST['price'] : null;
    }

    /**
     * Gets all entries of specified table as array of given objects
     * @param string $class classname
     * @param string $table db table name
     * @return array of $class
     */
    protected static function getAllEntries(string $class, string $table)
    {
        $stmt = parent::connect()->prepare("SELECT * FROM $table");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);
    }


    /**
     * Get all entries from db of class
     */
    abstract public static function getAll();

    /**
     * Inserts class data in Db table
     */
    abstract public function insertMeIntoDb();

    /**
     * Delete class from Db table
     */
    abstract public function deleteMeFromDb();

    /**
     * Display class in html
     */
    abstract public function display();
}
