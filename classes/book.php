<?php

//Comments in Product
class Book extends Product
{
    public $weight;
    private static $table = 'books';

    function __construct()
    {
        $this->weight = isset($_POST['weight']) ? $_POST['weight'] : null;
        parent::__construct();
    }

    public static function getAll()
    {
        return parent::getAllEntries(static::class, self::$table);
    }

    public function insertMeIntoDb()
    {
        $query = "INSERT INTO " . self::$table . " (`sku`,`name`,`price`,`weight`) VALUES (:sku, :name, :price, :weight)";
        parent::execute($query, [
            "sku" => $this->sku,
            "name" => $this->name,
            "price" => $this->price,
            "weight" => $this->weight,
        ]);
    }

    public function deleteMeFromDb()
    {
        parent::execute("DELETE FROM " . self::$table . " WHERE id = $this->id");
    }

    public function display()
    {
        $html = "<ul>
                    <li> $this->sku </li>
                    <li> $this->name </li>
                    <li> $this->price $</li>
                    <li>Weight: $this->weight KG</li>
                </ul>";
        echo $html;
    }
}
