<?php

/**
 * Database connection
 */
class Dbh
{
    private static $servername = 'localhost';
    private static $username = 'root';
    private static $password = 'toor';
    private static $dbname = 'products';

    /**
     * @return connection
     */
    protected static function connect()
    {
        try {
            $string = "mysql:host=" . self::$servername . ";dbname=" . self::$dbname;
            $pdo = new PDO($string, self::$username, self::$password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    /**
     * Excecutes given query 
     * @param string $query Query to execute
     * @param array $data Data for query
     */
    protected static function execute(string $query, array $data = [])
    {
        print_r($data);
        try {
            $stmt = self::connect()->prepare($query);
            $stmt->execute($data);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
