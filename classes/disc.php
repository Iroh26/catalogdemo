<?php

//Comments in Product
class Disc extends Product
{
    public $size;
    private static $table = 'discs';

    function __construct()
    {
        $this->weight = isset($_POST['size']) ? $_POST['size'] : null;
        parent::__construct();
    }

    public static function getAll()
    {
        return parent::getAllEntries(static::class, self::$table);
    }

    public function insertMeIntoDb()
    {
        $query = "INSERT INTO " . self::$table . " (`sku`,`name`,`price`,`size`) VALUES (:sku, :name, :price, :size)";
        parent::execute($query, [
            "sku" => $this->sku,
            "name" => $this->name,
            "price" => $this->price,
            "size" => $this->weight,
        ]);
    }

    public function deleteMeFromDb()
    {
        parent::execute("DELETE FROM " . self::$table . " WHERE id = $this->id");
    }

    public function display()
    {
        $html = "<ul>
                    <li> $this->sku </li>
                    <li> $this->name </li>
                    <li> $this->price $</li>
                    <li>Size: $this->size  MB</li>
                </ul>";
        echo $html;
    }
}
