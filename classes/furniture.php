<?php

//Comments in Product
class Furniture extends Product
{
    public $height;
    public $width;
    public $length;
    private static $table = 'furnitures';

    function __construct()
    {
        $this->height = isset($_POST['height']) ? $_POST['height'] : null;
        $this->width = isset($_POST['width']) ? $_POST['width'] : null;
        $this->length = isset($_POST['length']) ? $_POST['length'] : null;
        parent::__construct();
    }

    public static function getAll()
    {
        return parent::getAllEntries(static::class, self::$table);
    }

    public function insertMeIntoDb()
    {
        $query = "INSERT INTO " . self::$table . " (`sku`,`name`,`price`,`height`, `width`, `length`) VALUES (:sku, :name, :price, :height, :width, :length)";
        parent::execute($query, [
            "sku" => $this->sku,
            "name" => $this->name,
            "price" => $this->price,
            "height" => $this->height,
            "width" => $this->width,
            "length" => $this->length,
        ]);
    }

    public function deleteMeFromDb()
    {
        parent::execute("DELETE FROM " . self::$table . " WHERE id = $this->id");
    }

    public function display()
    {
        $html = "<ul>
                    <li> $this->sku </li>
                    <li> $this->name </li>
                    <li> $this->price $</li>
                    <li>Dimensions: $this->height" . "x" . "$this->width" . "x" . "$this->length (HxWxL) CM</li>
                </ul>";
        echo $html;
    }
}
