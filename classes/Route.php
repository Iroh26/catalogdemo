<?php

class Route
{
    public static $validRoutes = [];

    public static function set($route, $function)
    {
        self::$validRoutes[] = $route;

        $url = explode('/', $_SERVER['REQUEST_URI']);
        //print_r(in_array($url[1], self::$validRoutes));
        if (in_array($url[1], self::$validRoutes)) {
            if ($route == $url[1]) {
                $function->__invoke();
            }
        } else {
            header("HTTP/1.0 404 Not Found");
        }
    }
}
