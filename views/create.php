<div class="top">
    <h1>Product Add</h1>
    <div>
        <a href="/"><button>Back</button></a>
        <button form="create" type="submit">Save</button>
    </div>
</div>
<hr>
<form method="post" id="create" onsubmit="return validateForm()" class="create__form">
    <div class="field">
        <label for="sku">SKU: </label>
        <div>
            <input type="text" name="sku" required>
        </div>
    </div>
    <div class="field">
        <label for="name">Name: </label>
        <div>
            <input type="text" name="name" required>
        </div>
    </div>
    <div class="field">
        <label for="price">Price: </label>
        <div>
            <input type="number" step="0.01" min="0" name="price" required>
            <span class="input-dollar"></span>
        </div>
    </div>
    <div class="field">
        <label for="type">Category: </label>
        <div>
            <select name="type" id="productTypes" required>
                <option value="book">Book</option>
                <option value="disc">Disc</option>
                <option value="furniture">Furniture</option>
            </select>
        </div>
    </div>
    <div id="typeSpecs">
        <div id="book">
            <div class="field">
                <label for="weight">Weight: </label>
                <div>
                    <input type="number" step="0.01" min="0" name="weight" required>
                    <span class="subtext">Please specify books weight in kg.</span>
                </div>
            </div>
        </div>

        <div id="disc">
            <div class="field">
                <label for="size">Size: </label>
                <div>
                    <input type="number" step="0.01" min="0" name="size">
                    <span class="subtext">Please specify DVD-Discs size in mb.</span>
                </div>
            </div>
        </div>

        <div id="furniture">
            <div class="field">
                <label for="height">Height: </label>
                <div>
                    <input type="number" step="0.01" min="0" name="height">
                </div>
            </div>
            <div class="field">
                <label for="width">Width: </label>
                <div>
                    <input type="number" step="0.01" min="0" name="width">
                </div>
            </div>
            <div class="field">
                <label for="length">Length: </label>
                <div>
                    <input type="number" step="0.01" min="0" name="length">
                    <span class="subtext">Please specify furnitures dimensions in cm.</span>
                </div>
            </div>
        </div>
    </div>
</form>