<div class="top">
    <h1>Product List</h1>
    <form action="" class="top__right" id="actionsForm" method="post">
        <select name="action">
            <option value="create">Create</option>
            <option value="delete">Delete</option>
        </select>
        <button type="submit">Apply</button>
    </form>
</div>
<hr>
<div class="list">
    <?php foreach ($products as $i => $prod) : ?>
        <div class="item">
            <input class="item_checkbox" form="actionsForm" type="checkbox" name="products[]" value="<?= $i ?>">
            <?php $prod->display() ?>
        </div>
    <?php endforeach; ?>
</div>