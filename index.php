<?php
define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('CLASSES', ROOT . 'classes' . DIRECTORY_SEPARATOR);
define('CONTROLLERS', ROOT . 'controllers' . DIRECTORY_SEPARATOR);
define('VIEWS', ROOT . 'views' . DIRECTORY_SEPARATOR);
define('LAYOUTS', ROOT . 'views' . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($class) {
    if (file_exists(CLASSES . $class . '.php'))
        include_once(CLASSES . $class . '.php');
    else
        include_once(CONTROLLERS . $class . '.php');
});

include('Routes.php');
